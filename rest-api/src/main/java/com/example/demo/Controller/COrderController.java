package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.OrdersList;
import com.example.demo.Respository.iOrderListRespository;

@RestController
@CrossOrigin
public class COrderController {
    

    @Autowired
    iOrderListRespository iOrderList ;

    @GetMapping("orders")
    public ResponseEntity <List <OrdersList>> getOrderlist(){
        try {
            List<OrdersList> listOrder = new ArrayList<OrdersList>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            iOrderList.findAll().forEach(listOrder :: add);

            if(listOrder.size() == 0){
                return new ResponseEntity<List <OrdersList>>(listOrder, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <OrdersList>>(listOrder, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
